package com.houyewei.incometax;

import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.houyewei.incometax.dummy.DummyContent;

public class MainActivity extends AppCompatActivity implements ItemFragment.OnListFragmentInteractionListener, CalcFragment.OnFragmentInteractionListener
{

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {
        Toast.makeText(getApplicationContext(), item.content, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Toast.makeText(getApplicationContext(), uri.toString(), Toast.LENGTH_SHORT).show();
    }

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });



        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final String ARG_SECTION_NAME = "section_name";

        private static final int START_LEVEL = 1;
        private int mLevel;
        private Button mNextLevelButton;
        private InterstitialAd mInterstitialAd;
        private TextView mLevelTextView;


        private InterstitialAd newInterstitialAd() {
            InterstitialAd interstitialAd = new InterstitialAd(getContext());
            interstitialAd.setAdUnitId(getString(R.string.page_ad_unit_id));
            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    mNextLevelButton.setEnabled(true);
                    Toast.makeText(getContext(), "onAdLoaded", Toast.LENGTH_SHORT);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    mNextLevelButton.setEnabled(true);
                    Toast.makeText(getContext(), "onAdFiledToload", Toast.LENGTH_SHORT);
                }

                @Override
                public void onAdClosed() {
                    // Proceed to the next level.
                    goToNextLevel();
                }
            });
            return interstitialAd;
        }

        private void showInterstitial() {
            // Show the ad if it's ready. Otherwise toast and reload the ad.
            if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
                Toast.makeText(getContext(), "Ad did not load", Toast.LENGTH_SHORT).show();
                goToNextLevel();
            }
        }

        private void loadInterstitial() {
            // Disable the next level button and load the ad.
            AdRequest adRequest = new AdRequest.Builder()
                    .setRequestAgent("android_studio:ad_template").build();
            mInterstitialAd.loadAd(adRequest);
        }


        private void goToNextLevel() {
            // Show the next level and reload the ad to prepare for the level after.
            mLevelTextView.setText("123");
            mInterstitialAd = newInterstitialAd();
            loadInterstitial();
        }

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber, String sectionName) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putString(ARG_SECTION_NAME, sectionName);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            String sectionName = getArguments().getString(ARG_SECTION_NAME);
            int id = getResources().getIdentifier(sectionName, "layout", "com.houyewei.incometax");
            System.out.print(R.layout.fragment_about);
            View rootView = inflater.inflate(id, container, false);
            //View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)) + sectionName);


            // Create the next level button, which tries to show an interstitial when clicked.
            mNextLevelButton = ((Button) rootView.findViewById(R.id.next_level_button));
            mNextLevelButton.setEnabled(false);
            mNextLevelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInterstitial();
                }
            });

            // Create the text view to show the level number.
            mLevelTextView = (TextView) rootView.findViewById(R.id.level);
            mLevel = START_LEVEL;

            // Create the InterstitialAd and set the adUnitId (defined in values/strings.xml).
            mInterstitialAd = newInterstitialAd();
            loadInterstitial();
            showInterstitial();
            // Toasts the test ad message on the screen. Remove this after defining your own ad unit ID.
            Toast.makeText(getContext(), "Loaded", Toast.LENGTH_LONG).show();




            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            String fragName = "";
            switch (position) {
                case 0:
                    return CalcFragment.newInstance();
                case 1:
                    fragName = "fragment_main";
                    return ItemFragment.newInstance(1);
                default:
                    fragName = "fragment_about";
            }
            return PlaceholderFragment.newInstance(position + 1, fragName);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "个税计算";
                case 1:
                    return "常见问题";
                case 2:
                    return "关于我们";
            }
            return null;
        }
    }
}
